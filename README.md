# Foundry VTT D&D 5e 系统正體中文化 & 部分模組中文化
![page](./images/3.0page.webp)

## 支援模組
- [Argon - Combat HUD](https://foundryvtt.com/packages/enhancedcombathud)
- [Babele](https://foundryvtt.com/packages/babele)
- [Carousel Combat Tracker](https://foundryvtt.com/packages/combat-tracker-dock)
- [Health Estimate](https://github.com/mclemente/healthEstimate)
- [Midi Quality of Life Improvementsl](https://foundryvtt.com/packages/midi-qol)
- [Module Management+](https://foundryvtt.com/packages/module-credits)
- [Monk's Active Tile Triggers](https://foundryvtt.com/packages/monks-active-tiles)
- [Monk's Enhanced Journal](https://foundryvtt.com/packages/monks-enhanced-journal)
- [Monk's TokenBar](https://foundryvtt.com/packages/monks-tokenbar)
- [Smart-Target](https://github.com/theripper93/Smart-Target)
- [Token Action HUD D&D 5e](https://foundryvtt.com/packages/token-action-hud-dnd5e)
- [Tokenizer](https://foundryvtt.com/packages/vtta-tokenizer)

## 鳴謝
**感谢其他原中文化版本譯者**
